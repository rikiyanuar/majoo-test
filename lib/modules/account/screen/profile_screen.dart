import 'package:flutter/material.dart';
import 'package:majoo/core/utils/style/text_styles.dart';
import 'package:majoo/modules/account/state/profile_state.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ProfileState>(
      create: (BuildContext context) => ProfileState(
        context: context,
      ),
      child: Consumer<ProfileState>(
        builder: (_, ProfileState profileState, __) {
          return Column(children: [
            Container(
              padding: const EdgeInsets.all(8),
              margin: const EdgeInsets.only(top: kToolbarHeight, bottom: 24),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(.1),
                    blurRadius: 6,
                    offset: const Offset(0, 0),
                    spreadRadius: 6,
                  )
                ],
              ),
              child: Icon(
                Icons.person_rounded,
                color: Colors.grey,
                size: MediaQuery.of(context).size.width * 0.3,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
              child: Row(children: [
                const Text("Nama"),
                Text(profileState.accountEntity.name!,
                    style: TextStyles.blackRegularSemiBold),
              ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
            ),
            const Divider(height: 1),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
              child: Row(children: [
                const Text("Email"),
                Text(profileState.accountEntity.email!,
                    style: TextStyles.blackRegularSemiBold),
              ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () => profileState.logout(),
              child: Container(
                margin: const EdgeInsets.all(24),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(14),
                ),
                alignment: Alignment.center,
                padding: const EdgeInsets.all(14),
                child: Text(
                  "LOGOUT",
                  style: TextStyles.whiteRegularBold,
                ),
              ),
            ),
          ]);
        },
      ),
    );
  }
}
