import 'package:flutter/material.dart';
import 'package:majoo/core/utils/style/text_styles.dart';
import 'package:majoo/core/widgets/clippers/bottom_wave_clipper.dart';
import 'package:majoo/modules/account/state/registration_state.dart';
import 'package:provider/provider.dart';

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<RegistrationState>(
      create: (BuildContext context) => RegistrationState(
        context: context,
      ),
      child: Consumer<RegistrationState>(
        builder: (_, RegistrationState registrationState, __) {
          return Scaffold(
            backgroundColor: Colors.grey.shade50,
            body: Stack(children: [
              ClipPath(
                clipper: BottomWaveClipper(),
                child: Container(
                  color: Theme.of(context).primaryColor,
                  height: MediaQuery.of(context).size.height * 0.45,
                ),
              ),
              SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 32,
                    vertical: (MediaQuery.of(context).size.height * 0.4) * 0.7,
                  ),
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(18),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.black12,
                        blurRadius: 8,
                        offset: Offset(0, 0),
                        spreadRadius: 8,
                      )
                    ],
                  ),
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 24, bottom: 8),
                      child: Text(
                        "Create your account",
                        style: TextStyles.blackLargeSemiBold,
                      ),
                    ),
                    TextFormField(
                      controller: registrationState.inputName,
                      keyboardType: TextInputType.name,
                      autofocus: true,
                      decoration: const InputDecoration(
                        labelText: "Full Name",
                        hintText: "John Doe",
                      ),
                    ),
                    TextFormField(
                      controller: registrationState.inputEmail,
                      keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        labelText: "Email Address",
                        hintText: "aku@kamu.com",
                      ),
                    ),
                    TextFormField(
                      controller: registrationState.inputPassword,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      decoration: const InputDecoration(
                        labelText: "Password",
                        hintText: "*********",
                      ),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => registrationState.clickRegistration(),
                      child: Container(
                        margin: const EdgeInsets.only(top: 12),
                        decoration: BoxDecoration(
                          color: Colors.indigo,
                          borderRadius: BorderRadius.circular(14),
                        ),
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(14),
                        child: Text(
                          "SIGN UP",
                          style: TextStyles.whiteRegularBold,
                        ),
                      ),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => registrationState.gotoLogin(),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 28),
                        child: Text(
                          "Have an account? Login now",
                          style: TextStyles.pinkRegularSemiBold,
                        ),
                      ),
                    ),
                  ], mainAxisSize: MainAxisSize.min),
                ),
              ),
            ]),
          );
        },
      ),
    );
  }
}
