import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:majoo/core/entity/account_entity.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/modules/intro/screen/splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileState with ChangeNotifier {
  final BuildContext context;
  bool mounted = false;
  AccountEntity accountEntity = AccountEntity(name: "", email: "");

  ProfileState({required this.context}) {
    getProfile();
  }

  @override
  void dispose() {
    super.dispose();
    mounted = true;
  }

  notify() {
    if (!mounted) {
      notifyListeners();
    }
  }

  logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const SplashScreen()),
      (route) => false,
    );
  }

  getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = prefs.getString(Constants.accountPrefs);
    accountEntity = AccountEntity.fromJson(jsonDecode(data!));
    notify();
  }
}
