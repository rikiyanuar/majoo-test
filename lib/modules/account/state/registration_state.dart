import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/modules/account/screen/login_screen.dart';
import 'package:majoo/modules/layout/screen/layout_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegistrationState with ChangeNotifier {
  final BuildContext context;
  final inputName = TextEditingController();
  final inputEmail = TextEditingController();
  final inputPassword = TextEditingController();

  RegistrationState({required this.context});

  clickRegistration() async {
    if (inputEmail.text.isEmpty ||
        inputPassword.text.isEmpty ||
        inputName.text.isEmpty) {
      final snackBar = SnackBar(
        content: Container(
          decoration: BoxDecoration(
            color: Colors.red,
            borderRadius: BorderRadius.circular(4),
          ),
          padding: const EdgeInsets.all(16),
          child: const Text("Lengkapi form terlebih dahulu"),
        ),
        backgroundColor: Colors.transparent,
        padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 24),
        elevation: 0,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return;
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var data = {
      "name": inputName.text,
      "email": inputEmail.text,
    };

    prefs.setString(Constants.accountPrefs, jsonEncode(data));

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const LayoutScreen()),
      (route) => false,
    );
  }

  gotoLogin() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const LoginScreen()),
      (route) => false,
    );
  }
}
