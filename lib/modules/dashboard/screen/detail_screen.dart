import 'package:flutter/material.dart';
import 'package:majoo/core/entity/people_entity.dart';
import 'package:majoo/core/utils/style/text_styles.dart';
import 'package:majoo/modules/dashboard/state/detail_state.dart';
import 'package:provider/provider.dart';

class DetailScreen extends StatelessWidget {
  final People people;

  const DetailScreen({Key? key, required this.people}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DetailState>(
      create: (BuildContext context) => DetailState(
        context: context,
      ),
      child: Consumer<DetailState>(
        builder: (_, DetailState detailState, __) {
          return Scaffold(
            appBar: AppBar(
              title: const Text("Detail Data"),
              elevation: 0,
            ),
            body: SingleChildScrollView(
              child: Column(children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
                  child: Row(children: [
                    const Text("Nama"),
                    Text(people.name!, style: TextStyles.blackRegularSemiBold),
                  ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
                ),
                const Divider(height: 1),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
                  child: Row(children: [
                    const Text("Height"),
                    Text(people.height!,
                        style: TextStyles.blackRegularSemiBold),
                  ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
                ),
                const Divider(height: 1),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
                  child: Row(children: [
                    const Text("Mass"),
                    Text(people.mass!, style: TextStyles.blackRegularSemiBold),
                  ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
                ),
                const Divider(height: 1),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
                  child: Row(children: [
                    const Text("Films"),
                    Expanded(
                      child: Text(
                        detailState.formatListString(people.films!),
                        style: TextStyles.blackRegularSemiBold,
                      ),
                    ),
                  ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
                ),
                const Divider(height: 1),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 14),
                  child: Row(children: [
                    const Text("Species"),
                    Expanded(
                      child: Text(
                        detailState.formatListString(people.species!),
                        style: TextStyles.blackRegularSemiBold,
                      ),
                    ),
                  ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => detailState.deletePeople(people.id!),
                  child: Container(
                    margin: const EdgeInsets.all(24),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(14),
                    ),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(14),
                    child: Text(
                      "DELETE",
                      style: TextStyles.whiteRegularBold,
                    ),
                  ),
                ),
              ]),
            ),
          );
        },
      ),
    );
  }
}
