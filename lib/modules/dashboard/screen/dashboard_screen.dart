import 'package:flutter/material.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/modules/dashboard/state/dashboard_state.dart';
import 'package:provider/provider.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DashboardState>(
      create: (BuildContext context) => DashboardState(
        context: context,
      ),
      child: Consumer<DashboardState>(
        builder: (_, DashboardState dashboardState, __) {
          return Column(children: [
            AppBar(
              title: const Text(Constants.menuDashboard),
              elevation: 0,
              backgroundColor: Colors.transparent,
              actions: [
                IconButton(
                  onPressed: () => dashboardState.changeViewMode(),
                  icon: Icon(dashboardState.viewMode == 'list'
                      ? Icons.grid_view_rounded
                      : Icons.view_list_rounded),
                  color: Colors.white,
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(14, 0, 14, 14),
              padding: const EdgeInsets.symmetric(horizontal: 14),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.grey.shade300),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(.1),
                      blurRadius: 4,
                      offset: const Offset(0, 0),
                      spreadRadius: 4,
                    )
                  ]),
              child: TextFormField(
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: "Cari berdasarkan nama ...",
                ),
                onChanged: (value) => dashboardState.listenInput(value),
              ),
            ),
            Expanded(
              child: dashboardState.viewMode == 'list'
                  ? Container(
                      color: Colors.white,
                      child: MediaQuery.removePadding(
                        context: context,
                        removeTop: true,
                        child: listViewMode(dashboardState),
                      ),
                    )
                  : Container(
                      color: Colors.grey.shade50,
                      child: MediaQuery.removePadding(
                        context: context,
                        removeTop: true,
                        child: gridViewMode(dashboardState),
                      ),
                    ),
            )
          ]);
        },
      ),
    );
  }

  Widget listViewMode(DashboardState dashboardState) {
    final peopleList = dashboardState.searchResult.isEmpty
        ? dashboardState.peopleList
        : dashboardState.searchResult;
    return ListView.separated(
      itemBuilder: (context, index) {
        final people = peopleList[index];
        return ListTile(
          title: Text(people.name!),
          leading: IconButton(
            onPressed: () => dashboardState.addToFavorite(people: people),
            icon: Icon(people.isFavorite!
                ? Icons.favorite_rounded
                : Icons.favorite_outline_rounded),
            color: Colors.red,
          ),
          trailing: const Icon(Icons.chevron_right_rounded),
          onTap: () => dashboardState.gotoDetail(people: people),
        );
      },
      separatorBuilder: (context, index) {
        return const Divider(height: 1);
      },
      itemCount: peopleList.length,
    );
  }

  Widget gridViewMode(DashboardState dashboardState) {
    final peopleList = dashboardState.searchResult.isEmpty
        ? dashboardState.peopleList
        : dashboardState.searchResult;
    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 2.2,
      crossAxisSpacing: 2,
      mainAxisSpacing: 2,
      children: peopleList.map((people) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () => dashboardState.gotoDetail(people: people),
          child: Container(
            color: Colors.white,
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text(people.name!),
              ),
              IconButton(
                onPressed: () => dashboardState.addToFavorite(people: people),
                icon: Icon(people.isFavorite!
                    ? Icons.favorite_rounded
                    : Icons.favorite_outline_rounded),
                color: Colors.red,
                iconSize: 20,
              )
            ]),
          ),
        );
      }).toList(),
    );
  }
}
