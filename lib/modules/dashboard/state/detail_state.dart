import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:majoo/core/utils/service/sqlite_service.dart';

class DetailState with ChangeNotifier {
  final BuildContext context;
  bool mounted = false;

  DetailState({required this.context});

  @override
  void dispose() {
    super.dispose();
    mounted = true;
  }

  notify() {
    if (!mounted) {
      notifyListeners();
    }
  }

  formatListString(String data) {
    final list = jsonDecode(data);
    // List a = List.castFrom(list).toList();
    // for (var item in a) {
    //   print(item);
    // }
    return list.toString();
    // if (list.isEmpty) return "";
    // return list.join("\n");
  }

  deletePeople(String id) async {
    SqlLiteService sqlLiteService = SqlLiteService();
    await sqlLiteService.deleteOne(id);
    Navigator.pop(context);
  }
}
