import 'package:flutter/material.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/modules/account/screen/login_screen.dart';
import 'package:majoo/modules/layout/screen/layout_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashState with ChangeNotifier {
  final BuildContext context;

  SplashState({required this.context});

  checkLoginStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.getString(Constants.accountPrefs) == null) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const LoginScreen()),
        (route) => false,
      );
    } else {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const LayoutScreen()),
        (route) => false,
      );
    }
  }
}
