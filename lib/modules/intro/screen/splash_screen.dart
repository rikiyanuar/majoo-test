import 'package:flutter/material.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/core/utils/style/text_styles.dart';
import 'package:majoo/modules/intro/state/splash_state.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SplashState>(
      create: (BuildContext context) => SplashState(
        context: context,
      )..checkLoginStatus(),
      child: Consumer<SplashState>(
        builder: (_, SplashState splashState, __) {
          return Scaffold(
            body: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.pink,
              alignment: Alignment.center,
              child: Text(
                Constants.appTitle,
                style: TextStyles.whiteExtraLargeSemiBold,
              ),
            ),
          );
        },
      ),
    );
  }
}
