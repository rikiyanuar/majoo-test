import 'package:flutter/material.dart';
import 'package:majoo/modules/account/screen/profile_screen.dart';
import 'package:majoo/modules/dashboard/screen/dashboard_screen.dart';
import 'package:majoo/modules/favorite/screen/favorite_screen.dart';

class LayoutState with ChangeNotifier {
  final BuildContext context;
  int tabIndex = 0;
  bool mounted = false;

  LayoutState({required this.context});

  @override
  void dispose() {
    super.dispose();
    mounted = true;
  }

  notify() {
    if (!mounted) {
      notifyListeners();
    }
  }

  changeTab(int index) {
    tabIndex = index;
    notify();
  }

  getContent() {
    switch (tabIndex) {
      case 0:
        return const DashboardScreen();
      case 1:
        return const FavoriteScreen();
      case 2:
        return const ProfileScreen();
      default:
        return const DashboardScreen();
    }
  }
}
