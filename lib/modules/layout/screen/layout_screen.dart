import 'package:flutter/material.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/core/widgets/clippers/bottom_wave_clipper.dart';
import 'package:majoo/modules/layout/state/layout_state.dart';
import 'package:provider/provider.dart';

class LayoutScreen extends StatelessWidget {
  const LayoutScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LayoutState>(
      create: (BuildContext context) => LayoutState(
        context: context,
      ),
      child: Consumer<LayoutState>(
        builder: (_, LayoutState layoutState, __) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Stack(children: [
              ClipPath(
                clipper: BottomWaveClipper(),
                child: Container(
                  color: Theme.of(context).primaryColor,
                  height: 45 + kToolbarHeight,
                ),
              ),
              layoutState.getContent(),
            ]),
            bottomNavigationBar: BottomNavigationBar(
              currentIndex: layoutState.tabIndex,
              onTap: layoutState.changeTab,
              items: const [
                BottomNavigationBarItem(
                  icon: Icon(Icons.home_outlined),
                  activeIcon: Icon(Icons.home_rounded),
                  label: Constants.menuDashboard,
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.favorite_outline_rounded),
                  activeIcon: Icon(Icons.favorite_rounded),
                  label: Constants.menuFavorite,
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline_rounded),
                  activeIcon: Icon(Icons.person),
                  label: Constants.menuProfile,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
