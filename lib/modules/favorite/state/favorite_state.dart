import 'package:flutter/material.dart';
import 'package:majoo/core/entity/people_entity.dart';
import 'package:majoo/core/utils/service/sqlite_service.dart';
import 'package:majoo/modules/dashboard/screen/detail_screen.dart';

class FavoriteState with ChangeNotifier {
  final BuildContext context;
  bool mounted = false;
  List<People> peopleList = [];

  FavoriteState({required this.context}) {
    getPeople();
  }

  @override
  void dispose() {
    super.dispose();
    mounted = true;
  }

  notify() {
    if (!mounted) {
      notifyListeners();
    }
  }

  getPeople() async {
    SqlLiteService sqlLiteService = SqlLiteService();
    peopleList = await sqlLiteService.getFavorite();
    notify();
  }

  addToFavorite({required People people}) async {
    SqlLiteService sqlLiteService = SqlLiteService();
    final newPeople = People(
      id: people.id,
      name: people.name,
      height: people.height,
      mass: people.mass,
      films: people.films,
      species: people.species,
      isFavorite: people.isFavorite! ? false : true,
    );
    await sqlLiteService.updateOne(newPeople);
    getPeople();
  }

  gotoDetail({required People people}) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailScreen(people: people),
      ),
    );
    getPeople();
  }
}
