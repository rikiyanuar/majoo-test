import 'package:flutter/material.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/modules/favorite/state/favorite_state.dart';
import 'package:provider/provider.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<FavoriteState>(
      create: (BuildContext context) => FavoriteState(
        context: context,
      ),
      child: Consumer<FavoriteState>(
        builder: (_, FavoriteState favoriteState, __) {
          return Column(children: [
            AppBar(
              title: const Text(Constants.menuFavorite),
              elevation: 0,
              backgroundColor: Colors.transparent,
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: listViewMode(favoriteState),
                ),
              ),
            )
          ]);
        },
      ),
    );
  }

  Widget listViewMode(FavoriteState favoriteState) {
    return ListView.separated(
      itemBuilder: (context, index) {
        final people = favoriteState.peopleList[index];
        return ListTile(
          title: Text(people.name!),
          leading: IconButton(
            onPressed: () => favoriteState.addToFavorite(people: people),
            icon: Icon(people.isFavorite!
                ? Icons.favorite_rounded
                : Icons.favorite_outline_rounded),
            color: Colors.red,
          ),
          trailing: const Icon(Icons.chevron_right_rounded),
          onTap: () => favoriteState.gotoDetail(people: people),
        );
      },
      separatorBuilder: (context, index) {
        return const Divider(height: 1);
      },
      itemCount: favoriteState.peopleList.length,
    );
  }
}
