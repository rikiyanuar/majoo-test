import 'package:flutter/material.dart';
import 'package:majoo/core/utils/service/sqlite_service.dart';

import 'core/widgets/app/index.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SqlLiteService();
  runApp(const MainApp());
}
