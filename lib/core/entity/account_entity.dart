class AccountEntity {
  final String? name, email;

  AccountEntity({this.name, this.email});

  factory AccountEntity.fromJson(Map<String, dynamic> json) => AccountEntity(
        name: json['name'],
        email: json['email'],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
      };
}
