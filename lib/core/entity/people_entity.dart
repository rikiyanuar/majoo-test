import 'dart:convert';

class PeopleEntity {
  final bool? status;
  final String? message;
  final PeopleList? result;

  PeopleEntity({this.status, this.message, this.result});

  factory PeopleEntity.fromJson(Map<String, dynamic> json) => PeopleEntity(
        status: json['status'],
        message: json['message'],
        result:
            json["result"] == null ? null : PeopleList.fromJson(json["result"]),
      );
}

class PeopleList {
  final List<People>? people;

  PeopleList({this.people});

  factory PeopleList.fromJson(Map<String, dynamic> json) => PeopleList(
        people: json["results"] == null
            ? null
            : List<People>.from(json["results"].map((x) => People.fromJson(x))),
      );
}

class People {
  final String? id, name, height, mass, films, species;
  final bool? isFavorite;

  People({
    this.id,
    this.name,
    this.height,
    this.mass,
    this.films,
    this.species,
    this.isFavorite,
  });

  factory People.fromJson(Map<String, dynamic> json) => People(
        name: json['name'],
        height: json['height'],
        mass: json['mass'],
        films: jsonEncode(json["films"]),
        species: jsonEncode(json["species"]),
        isFavorite: json['isFavorite'] ?? false,
      );

  factory People.fromMap(Map<String, dynamic> map) => People(
        id: map['id'].toString(),
        name: map['name'],
        height: map['height'],
        mass: map['mass'],
        films: map["films"],
        species: map["species"],
        isFavorite: map['isFavorite'] == 'true' ? true : false,
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "height": height,
        "mass": mass,
        "films": json.encode(films),
        "species": json.encode(species),
        "isFavorite": isFavorite.toString(),
      };
}
