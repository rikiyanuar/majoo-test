import 'package:majoo/core/entity/people_entity.dart';
import 'package:majoo/core/utils/constants/constants.dart';
import 'package:majoo/core/utils/service/api_service.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqlLiteService {
  SqlLiteService() {
    insertData();
  }

  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, Constants.dbName),
      onCreate: (database, version) async {
        await database.execute('''
          CREATE TABLE ${Constants.tblPeople} (
            id INTEGER PRIMARY KEY AUTOINCREMENT, 
            name TEXT, 
            height TEXT, 
            mass TEXT, 
            films TEXT, 
            species TEXT, 
            isFavorite TEXT
          ),
        ''');
      },
      version: 1,
    );
  }

  insertData() async {
    final db = await initializeDB();
    int? count = Sqflite.firstIntValue(
        await db.rawQuery("SELECT COUNT(*) FROM ${Constants.tblPeople}"));
    if (count == 0) {
      final dataPeople = await getPeopleData();
      for (People data in dataPeople) {
        db.insert(Constants.tblPeople, data.toJson());
      }
    }
  }

  getPeopleData() async {
    ApiService apiService = ApiService();
    Map<String, dynamic> response = await apiService.getAPI(url: "people");
    PeopleEntity peopleEntity = PeopleEntity.fromJson(response);
    return peopleEntity.result!.people;
  }

  deleteAll() async {
    final db = await initializeDB();
    db.delete(Constants.tblPeople);
  }

  getAll() async {
    final db = await initializeDB();
    final List<Map<String, dynamic>> maps = await db.query(Constants.tblPeople);
    return maps.map((e) => People.fromMap(e)).toList();
  }

  getFavorite() async {
    final db = await initializeDB();
    final List<Map<String, dynamic>> maps = await db.query(
      Constants.tblPeople,
      where: 'isFavorite = ?',
      whereArgs: [true],
    );
    return maps.map((e) => People.fromMap(e)).toList();
  }

  updateOne(People people) async {
    final db = await initializeDB();
    await db.update(
      Constants.tblPeople,
      people.toJson(),
      where: 'id = ?',
      whereArgs: [people.id],
    );
  }

  deleteOne(String id) async {
    final db = await initializeDB();
    await db.delete(
      Constants.tblPeople,
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
