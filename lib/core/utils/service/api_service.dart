import 'package:dio/dio.dart';
import 'package:majoo/core/utils/constants/constants.dart';

class ApiService {
  initDio() {
    final dio = Dio();
    dio.options.headers = {
      "Accept": "application/json",
    };
    dio.options.connectTimeout = 31000;
    dio.options.baseUrl = Constants.baseURL;
    return dio;
  }

  Future getAPI({required String url}) async {
    final dio = initDio();
    try {
      Response response = await dio.get(url);
      return {"status": true, "message": "OK", "result": response.data};
    } on DioError catch (e) {
      return {"status": false, "message": e.response!.data['message']};
    }
  }
}
