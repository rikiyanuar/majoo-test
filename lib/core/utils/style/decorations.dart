import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:majoo/core/utils/style/text_styles.dart';

class Decorations {
  static BoxDecoration card = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(8),
    boxShadow: [
      BoxShadow(
        color: Colors.black.withOpacity(.07),
        spreadRadius: 2,
        blurRadius: 6,
        offset: const Offset(0.0, 2.0),
      ),
    ],
  );

  static InputDecoration inputDecoration = InputDecoration(
    border: InputBorder.none,
    focusedErrorBorder: const UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.red),
    ),
    errorBorder: const UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.red),
    ),
    errorStyle: const TextStyle(fontSize: 11, height: .4),
    hintStyle: TextStyles.greyLargeSemiBold,
    contentPadding: EdgeInsets.zero,
  );
}
