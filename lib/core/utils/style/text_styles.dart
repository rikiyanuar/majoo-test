import 'package:flutter/material.dart';

class TextStyles {
  /// Regular Text
  static TextStyle blackRegularLight = const TextStyle(
    color: Colors.black,
    fontSize: 14,
    fontWeight: FontWeight.w300,
  );
  static TextStyle blackRegularSemiBold = const TextStyle(
    color: Colors.black,
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );
  static TextStyle whiteRegularBold = const TextStyle(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );
  static TextStyle pinkRegularNormal = const TextStyle(
    color: Colors.pink,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );
  static TextStyle pinkRegularSemiBold = const TextStyle(
    color: Colors.pink,
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );

  /// Medium Text
  static TextStyle greyMediumNormal = const TextStyle(
    color: Colors.grey,
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  /// Large Text
  static TextStyle blackLargeLight = const TextStyle(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w300,
  );
  static TextStyle blackLargeNormal = const TextStyle(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );
  static TextStyle blackLargeSemiBold = const TextStyle(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w500,
  );
  static TextStyle greyLargeSemiBold = const TextStyle(
    color: Colors.grey,
    fontSize: 18,
    fontWeight: FontWeight.w500,
  );

  /// Extra Large Text
  static TextStyle blackExtraLargeSemiBold = const TextStyle(
    color: Colors.black,
    fontSize: 22,
    fontWeight: FontWeight.w500,
  );
  static TextStyle redExtraLargeBold = const TextStyle(
    color: Colors.red,
    fontSize: 22,
    fontWeight: FontWeight.bold,
  );
  static TextStyle whiteExtraLargeSemiBold = const TextStyle(
    color: Colors.white,
    fontSize: 22,
    fontWeight: FontWeight.w500,
  );
}
