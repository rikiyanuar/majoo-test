class Constants {
  static const appTitle = "Majoo Test";
  static const baseURL = "https://swapi.dev/api/";
  static const menuDashboard = "Beranda";
  static const menuFavorite = "Favorit";
  static const menuProfile = "Profil";

  /// for shared prefs
  static const accountPrefs = "ACCOUNT_PREFS";

  /// for database
  static const dbName = "majoo.db";
  static const tblPeople = "people";
}
